**Description générale :**

En direction/provenant du [point de la branche]

Indiquer les tourne à droite/gauche

Indiquer les îlots, passage piéton, feux.

Piste cyclable

(Stationnement)

Indiquer la provenance/direction, puis préciser le nombre/type de voies

Fusionner les phrases de description de voies pour une rue à sens unique

**Description interactive :**

N° de rue (arrivée/sortie). Croissant/décroissant.

Distance en option + "floutée" (à une dizaine de m)

~~Enfin, poursuivez votre route à droite = Poursuivez alors votre route à droite~~

Lors d'une terminaison : "Enfin poursuivez votre à droite en direction de [...]"  → Rétablir le fonctionnement original : demi-tour sur le nœud.

Marcher derrière = Faire demi-tour et marcher tout droit.

Orientation du passage piéton (sens horaire ou parallèle à une voie)

Distance au prochain passage piéton

Il permet de traverser deux voies qui viennent de votre gauche/de votre droite

Prendre le passage piéton en direction de X. La traversée se fait en plusieurs fois.

Prendre le passage piéton = Traverser

Dans les choix, séparer le demi-tour des autres choix

Hiérarchiser les choix (dans l'ordre horaire : gauche, face, droite, ou face, gauche, droite)

~~Pour les directions multiples, remplacer ET par OU~~
