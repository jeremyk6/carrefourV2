var xml;

var player;

var graph;

var map;
var basemaps = {}
basemaps["OpenStreetMap"] = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {attribution: '© Contributeurs OpenStreetMap', maxNativeZoom: 19, maxZoom: 20})
basemaps["Google Maps"] = L.tileLayer('http://mt1.google.com/vt/lyrs=m&x={x}&y={y}&z={z}' , {attribution: '© Données cartographiques Google', maxZoom: 20})
basemaps["Google Satellite"] = L.tileLayer('https://mt1.google.com/vt/lyrs=s&x={x}&y={y}&z={z}', {attribution: '© Données cartographiques Google', maxZoom: 20})
arrow = L.icon({
    iconUrl: 'https://freesvg.org/img/kuba_arrow_button_set_1.png',
    iconSize: [30,30],
    iconAnchor: [15, 15]
})

/*
 * Init
 */
function init(data) {

    // Reading of the xml file
    xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function() {
        xml = xhr.responseXML;
        if(xml) {
            /* Objects creation */
            for(node of ["intersection", "branch","way","junction"]) {
                window[node] = []
                xnodes = XPathRequest(xml, "intersection"+ (node == "intersection" ? "":"/"+node))
                for(xnode of xnodes) {
                    obj = {}
                    for(attribute of xnode.attributes) {
                        value = attribute.value.split(',')
                        for(i = 0; i < value.length; i++) {
                            if(!isNaN(Number(value[i])))
                                value[i] = Number(value[i])
                        }
                        obj[attribute.name] = value.length > 1 ? value : value[0]
                    }
                    window[node].push(obj)
                }
            }

            /* Graph creation */
            graph_map = {}
            graph_pedestrian = {}
            for(node of junction.filter(j => j.type != "road_junction")) {
                graph_map[node.fid] = {}
                graph_pedestrian[node.fid] = {}
                for(w of way.filter(w => w.nodes.includes(node.fid))) {
                    n = w.nodes.filter(n => n != node.fid)
                    graph_map[node.fid][n] = w.length
                    graph_pedestrian[node.fid][n] = w.type == "crossing_line" ? 1 : 0
                }
            }
            graph = new Graph(graph_map)

            /* Map creation */
            map = L.map('map');
            map.fitBounds(L.latLngBounds(junction.map(j => [j.y, j.x])))
            Object.values(basemaps)[0].addTo(map);
            L.control.layers(basemaps).addTo(map);
            L.control.scale({imperial: false}).addTo(map);

            description(xml);
        }
    }
    xhr.open("GET",data,true);
    xhr.send();

    // jsRealB configuration
    loadFr();

    // New words for the lexicon
    addToLexicon("pyramide", {"N":{"g":"f","tab":["n17"]}})
    addToLexicon("viaduc", {"N":{"g":"m","tab":["n3"]}})
    addToLexicon("croisement", {"N":{"g":"m","tab":["n3"]}})
    addToLexicon("bus", {"N":{"g":"m","tab":["n3"]}})
    addToLexicon("îlot", {"N":{"g":"m","tab":["n3"]}})
    addToLexicon("tourne-à-gauche", {"N":{"g":"m","tab":["n3"]}})
    addToLexicon("tourne-à-droite", {"N":{"g":"m","tab":["n3"]}})
    addToLexicon("entrant", {"A":{"tab":["n28"]}})
    addToLexicon("sortant", {"A":{"tab":["n28"]}})
}

/*
 * General description
 */
function description(xml) {

    description = document.getElementById("description")

    if(!window.location.search) {

        /*
        * Description of the intersection's type
        */

        description.innerHTML += 
        S(
            Q("Le carrefour à l'intersection"),
            CP.apply(this,
                [C("et")].concat(
                    Array.from(unique(branch.map(b => b.name)), rue => 
                        PP(
                            P("de"), 
                            NP(
                                D("le"), 
                                N(rue[0]), 
                                Q(rue[1])
                            )
                        )
                    )
                )
            ),
            Q(intersection[0].type == undefined ? "est un carrefour composé de" : "est un carrefour en "+tr(intersection[0].type)+" composé de"),
            NP(
                NO(branch.length).dOpt({nat: true}), 
                N("branche")
            )
        ) + "<br/><br/>"

        /*
        * Description of intersection's branches
        */
        
        content = "<h3>Description des branches</h3><hr/>"

        branch.forEach(function(b, i) {
            b_ways = Array.from(b.ways, fid => way.find(w => w.fid == fid)).filter(w => ["road","service", "bus"].includes(w.type))
            outgoing = {}
            incoming = {}
            for(b_way of b_ways) {
                if(JSON.stringify(b_way.from) == JSON.stringify(b.direction)) {
                    // If going to the same direction of the branch, then it's an outgoing way
                    if(outgoing[b_way.type] == undefined) outgoing[b_way.type] = 0
                    outgoing[b_way.type]++
                } else {
                    // Else it's an incoming way
                    if(incoming[b_way.type] == undefined) incoming[b_way.type] = 0
                    incoming[b_way.type]++
                }
            }

            content += "<p>" +
            "La branche en direction " +
            PP(
                P("de"), 
                NP(
                    D("le"),
                    Array.isArray(b.direction) ? N(b.direction[0]) : N(b.direction),
                    Array.isArray(b.direction) ? Q(b.direction[1]) : Q('')
                ),
            ) +
            " qui s'appelle " +
            NP(
                D("le"),
                N(b.name[0]),
                Q(b.name[1])
            ) +
            " est composée "
            
            // If there's only either incoming or outgoing ways
            if(Object.keys(outgoing).length == 0 || Object.keys(incoming).length == 0) {

                object = Object.keys(outgoing).length == 0 ? incoming : outgoing
                word = Object.keys(outgoing).length == 0 ? " entrante" : " sortante"

                content += CP.apply(this,
                    [C("et")].concat(
                        Array.from(
                            Object.entries(object), ways => 
                            PP(
                                P("de"),
                                NP(
                                    NO(ways[1]).dOpt({nat: true}),
                                    N("voie"),
                                    PP(
                                        P("de"),
                                        NP(tr(ways[0]))
                                    )
                                )
                            )
                        )
                    )
                )
                if(Object.keys(object).length) content += (Object.values(object).reduce((a, b) => a + b,0) > 1) ? word+"s" : word
                content += "."
            
            // Else there are both incoming and outgoing ways
            } else {
          
                content += PP(
                    P("de"),
                    NP(
                        NO(b_ways.length).dOpt({nat: true}),
                        N("voie")
                    )
                ) + " : "
                
                // Outgoing ways sentence generation
                outgoing_sentence = CP.apply(this,
                    [C("et")].concat(
                        Array.from(
                            Object.entries(outgoing), ways => 
                            NP(
                                NO(ways[1]).dOpt({nat: true}),
                                N("voie"),
                                PP(
                                    P("de"),
                                    NP(tr(ways[0]))
                                )
                            )
                        )
                    )
                )
                if(Object.keys(outgoing).length) outgoing_sentence += (Object.values(outgoing).reduce((a, b) => a + b,0) > 1) ? " sortantes" : " sortante"
                
                // Incoming ways sentence generation
                incoming_sentence = CP.apply(this,
                    [C("et")].concat(
                        Array.from(
                            Object.entries(incoming), ways => 
                            NP(
                                NO(ways[1]).dOpt({nat: true}),
                                N("voie"),
                                PP(
                                    P("de"),
                                    NP(tr(ways[0]))
                                )
                            )
                        )
                    )
                )
                if(Object.keys(incoming).length) incoming_sentence += (Object.values(incoming).reduce((a, b) => a + b,0) > 1) ? " entrantes" : " entrante"
                
                // Sentences compilation
                content += CP(C("et"), outgoing_sentence, incoming_sentence) + "."
            }

            content += "</p>"   
        })

        /*
         * Description of crossings
         */

        content += "<h3>Traversées</h3><hr/>"

        branch.forEach(function(b, i) {
            content += "<p>La branche " + (Array.isArray(b.direction) ? b.direction[0] + " " + b.direction[1] : b.direction) + " "
            sidewalks = []
            for(way_id of b.ways) {
                w = way.find(w => w.fid == way_id)
                if(w != undefined && w.type == "sidewalk") sidewalks.push(w)
            }
            // The branch can only be crossed if there's two sidewalks
            if(sidewalks.length == 2) {
                n1 = sidewalks[0].nodes[0]
                n2 = sidewalks[1].nodes[1]
                path = Graph.findShortestPath(graph_pedestrian, n1, n2)
                path = pathAsWays(path, way)
                // Count the number of sidewalks and crossings covered
                n_sidewalks = 0
                n_crossing = 0
                no_tactile_paving = true
                missing_tactile_paving = false
                path.forEach(function(p,i) {
                    if(p.type == "sidewalk") { 
                        n_sidewalks++
                        if(i > 0 && path[i-1].type == "sidewalk") n_sidewalks--
                    }
                    if(p.type == "crossing_line") { 
                        n_crossing++;
                        if(p.tactile_paving=="no") missing_tactile_paving = true
                        else no_tactile_paving = false
                    }
                })
                // if we cover more than 2 sidewalks, the branch can't be crossed on its own
                if(n_sidewalks == 2) {
                    content += "se traverse en " + NP(NO(n_crossing).dOpt({nat: true}), N("fois")) + ". "
                    content += "Il " + (no_tactile_paving ? "n'y a pas de": missing_tactile_paving ? "manque des " : "y a des ") + "bandes d'éveil de vigilance."  
                    content += "</p>"
                } else {
                    content += "ne peut pas être traversée."
                }
            } else {
                content += "ne peut pas être traversée."
            }
            content += "</p>"
        })

        /*
         * Text addition to the page
         */

        description.innerHTML += content

        /*
         * Description of possible incomings into the intersection
         */

        description.innerHTML += "<h3>Description interactive</h3><hr/>"

        description.innerHTML += "D'où arrivez-vous ?<br/>"
        
        froms = unique(junction.filter(j => j.type == "start").map(s => s.from))
        for(from of froms) {
            content = "<ul>"

            content +=
            S(
                PP(
                    P("depuis"),
                    NP(
                        D("le"),
                        Array.isArray(from) ? N(from[0]) : N(from),
                        Array.isArray(from) ? Q(from[1]) : Q('')
                    )
                ),
                Q("sur le trottoir").a(":")
            )
            
            content += "<ul>"
            for(side of junction.filter(j => j.type == "start" && JSON.stringify(j.from) == JSON.stringify(from)).map(s => s.side)) {
                start = junction.find(j => j.type == "start" && JSON.stringify(j.from) == JSON.stringify(from) && j.side == side)
                w = way.find(w => w.nodes.includes(start.fid))
                content += "<li><a href=?i="+start.fid+"&r="+w.azimut+">"
                switch(side) {
                    case "left":
                        content += "De gauche."
                        break;
                    case "right":
                        content += "De droite."
                        break;
                        
                }
                content += "</a></li>"
            }
            content += "</ul>"
            
            content += "</ul>"

            description.innerHTML += content
        }
        
    } else {

        /*
         * Description interactive
         */

        params = new URLSearchParams(window.location.search);

        if(params.has("i") && !params.has("j")) {

            /* Point de départ */

            start = junction.find(j => j.fid == params.get("i"))

            description.innerHTML = 
            S(
                Q("Vous venez"),
                PP(
                  P("de"),
                  NP(
                      D("le"),
                      Array.isArray(start.from) ? N(start.from[0]) : N(start.from),
                      Array.isArray(start.from) ? Q(start.from[1]) : Q('')
                  )  
                ),
                P("par"),
                NP(
                    D("le"),
                    N(start.by[0]),
                    Q(start.by[1])
                ),
                Q("sur le trottoir de " + (start.side == "right" ? "droite" : "gauche"))
            )
        
            description.innerHTML += chooseDirection(start.fid, params.get("r"))
            L.marker([start.y, start.x], {icon: arrow, rotationAngle: Number(params.get("r"))+90}).addTo(map);

        } else if(params.has("i") && params.has("j")) {

            i_node = junction.find(j => j.fid == params.get("i"))
            j_node = junction.find(j => j.fid == params.get("j"))

            path = graph.findShortestPath(i_node.fid, j_node.fid)
            ways = []
            coords = []
            for(i = 0; i < path.length-1; i++) {
                n1 = junction.find(j => j.fid == Number(path[i]))
                n2 = junction.find(j => j.fid == Number(path[i+1]))
                w = way.find(w => w.nodes.includes(n1.fid) && w.nodes.includes(n2.fid))
                ways.push(w.fid)
                coords.push([n1.y, n1.x], [n2.y, n2.x])
            }
            ret = buildDescription(i_node, params.get("r"), ways)
            description.innerHTML = ret[0]
            description.innerHTML += chooseDirection(j_node.fid, ret[1])
            L.polyline(coords, {color: 'red'}).addTo(map);
            L.marker([j_node.y, j_node.x], {icon: arrow, rotationAngle: ret[1]+90}).addTo(map);

        } else {
            printError()
        }

    }

}

function printError() {
    document.getElementById("description").innerHTML = "Une erreur de paramètre a été détectée. Veuillez retourner à l'accueil"
}

function chooseDirection(start_fid, azimut) {

    choices = []

    ranks = {}
    next_nodes = []
    backlist = []

    for(next_node of getNextNodes(start_fid, graph_map, junction, ["crossing_io","start"])) {

        /* Copie locale du graphe pour modification */
        graph = {...graph_pedestrian}

        ranks[next_node.fid] = {}

        /* Si c'est un nœud de départ, une seule direction : sa provenance, et coût nul */
        if(next_node.type == "start") {
            ranks[next_node.fid][JSON.stringify(next_node.from)] = 0
            next_nodes.push(next_node)
            continue
        } 
        
        /* Cas du demi-tour, si l'arête parcourue est un passage piéton, on la coupe pour empêcher la recherche de chemin en arrière */
        if(graph_pedestrian[start_fid][next_node.fid] == 1) {
            delete graph[start_fid][next_node.fid]
            delete graph[next_node.fid][start_fid]
            backlist.push(next_node.fid)
        }
        /* On avance à l'extrémité du passage piéton et on coupe l'arête pour empêcher la recherche de chemin en arrière */
        else {
            w = way.find(w => w.type == "crossing_line" && w.nodes.includes(next_node.fid))
            new_node = w.nodes[0] == next_node.fid ? junction.find(j => j.fid == w.nodes[w.nodes.length - 1]) : junction.find(j => j.fid == w.nodes[0])
            delete graph[next_node.fid][new_node.fid]
            delete graph[new_node.fid][next_node.fid]
            delete ranks[next_node.fid]
            next_node = new_node
            ranks[next_node.fid] = {}
        }

        /* Pour chaque direction, on regarde quel est le coût le plus faible */
        /* Note : on a déjà traversé un passage piéton, on ajoute donc +1 au coût */
        for(direction of unique(junction.filter(j => j.type == "start").map(j => j.from))) {
            ranks[next_node.fid][JSON.stringify(direction)] = 99
            fids = junction.filter(j => j.type == "start" && JSON.stringify(j.from) == JSON.stringify(direction)).map(j => j.fid)
            for(fid  of fids) {
                route = Graph.findShortestPath(graph, next_node.fid, fid)
                if(route && getPathCost(graph, route)+1 < ranks[next_node.fid][JSON.stringify(direction)])
                    ranks[next_node.fid][JSON.stringify(direction)] = getPathCost(graph, route)+1
            }
        }

        next_nodes.push(next_node)
    }
    
    /* Ranking */
    for(direction of unique(junction.filter(j => j.type == "start").map(j => JSON.stringify(j.from)))) {
        // Récupération du score le plus faible pour une direction
        rank = 99
        for(next_node of next_nodes) {
            if(ranks[next_node.fid][direction] < rank) 
                rank = ranks[next_node.fid][direction]
        }
        // Suppression des rangs supérieurs
        for(next_node of next_nodes) {
            if(ranks[next_node.fid][direction] > rank) 
                delete ranks[next_node.fid][direction]
            if(Object.keys(ranks[next_node.fid]).length == 0)
                delete ranks[next_node.fid]
        }
    }

    /* On attribue aux nœuds sans direction les directions des premiers trottoirs qui leur sont accessibles */
    nranks = {}
    sidewalk_io = junction.filter(j => j.type == "crossing_io" && unique(way.filter(w => w.type == "sidewalk").flatMap(w => w.nodes)).includes(j.fid))
    for(next_node of next_nodes) {
        if(!ranks[next_node.fid]) {
            nranks[next_node.fid] = []
            /* Si on arrive déjà sur un trottoir */
            if(sidewalk_io.includes(next_node)) {
                nranks[next_node.fid] = unique(getNextNodes(Number(route[route.length-1]), graph, junction, ["crossing_io","start"]).filter(n => n.type == "start").map(n => JSON.stringify(n.from)))
            } else {
                /* On supprime le passage piéton que l'on vient d'emprunter */
                graph = {...graph_map}
                crossing_line = way.find(w => w.type == "crossing_line" && w.nodes.includes(next_node.fid)).nodes
                delete graph[crossing_line[0]][crossing_line[1]]
                delete graph[crossing_line[1]][crossing_line[0]]
                /* On recherche les chemins vers les trottoirs */
                routes = []
                for(sidewalk of sidewalk_io) {
                    routes.push(Graph.findShortestPath(graph, next_node.fid, sidewalk.fid))
                }
                /* On élimine les chemins qui vont au-delà des trottoirs */
                for(route of routes) {
                    direct = true
                    for(i = 0; i < route.length-2; i++) {
                        line = way.find(w => w.nodes.includes(Number(route[i])) && w.nodes.includes(Number(route[i+1])))
                        if(line.type == "sidewalk") {
                            direct = false
                        }
                    }
                    if(direct) {
                        nranks[next_node.fid] = nranks[next_node.fid].concat(getNextNodes(Number(route[route.length-1]), graph, junction, ["crossing_io","start"]).filter(n => n.type == "start").map(n => JSON.stringify(n.from)))
                    }
                }
                nranks[next_node.fid] = unique(nranks[next_node.fid])
            }
        }
    }

    /* Création des choix principaux */
    for(fid of Object.keys(ranks)) {
        directions = []
        for(direction of Object.keys(ranks[fid])) {
            directions.push(JSON.parse(direction))
        }
        choices.push([junction.find(j => j.fid==fid), directions])
    }

    /* Création des choix secondaires */
    nchoices = []
    for(fid of Object.keys(nranks)) {
        directions = []
        for(direction of Object.keys(nranks[fid])) {
            directions.push(JSON.parse(direction))
        }
        nchoices.push([junction.find(j => j.fid==fid), directions])
    }

    /* Génération des phrases de choix */
    content = "<br/><br/>Vous pouvez : <ul>"
    for(choice of choices) {
        back = false
        if(backlist.includes(choice[0].fid)) {
            nchoices.push(choice)
            continue
        }
        if(junction.find(n => n.fid == start_fid).type == "start" && params.has("j"))
            back = true
        content += buildChoices(start_fid, choice, azimut, back)
    }
    content += "</ul>"
    if(nchoices.length > 0) {
        content += "Vous pouvez aussi : <ul>"
        for(choice of nchoices) {
            back = false
            if(backlist.includes(choice[0].fid))
                back = true
            content += buildChoices(start_fid, choice, azimut, back)
        }
        content += "</ul>"
    }

    return(content)
}


/* Fonction de génération des phrases de choix */
function buildChoices(start_fid, choice, azimut, back) {
    content = ""
    i = start_fid
    j = choice[0].fid
    content += "<li><a href=?i="+start_fid+"&j="+j+"&r="+azimut+">"
    if(choice[0].type == "start") {
        content += (back ? "Faire demi-tour" : "Continuer") + " sur le trottoir en direction "
    }
    else {
        content += (back ? "Faire demi-tour sur" : "Prendre") + " le passage piéton en direction "
    }
    content += 
        CP.apply(this,
            [C("ou")].concat(
                Array.from(choice[1], direction => 
                    PP(
                        P("de"), 
                        NP(
                            D("le"),
                            Array.isArray(direction) ? N(direction[0]) : N(direction),
                            Array.isArray(direction) ? Q(direction[1]) : Q('')
                        )  
                    )
                )
            )
        )
    content += "</a></li>"
    return(content)
}

function buildDescription(start_junction, azimut, ways) {
    current_node = start_junction
    fl = true; // true s'il s'agit de la première itération de la boucle
    content = ""
    for(fid of ways) {

        w = {...way.find(w => w.fid == fid)} // copie de la way courante pour la manipuler à l'envi

        // On passe les arêtes virtuelles
        if(w.type == "virtual") continue;

        // Inversion des nœuds de l'arête si parcourue à l'envers
        if(w.nodes[0] != current_node.fid) {
            w.nodes[1] = [w.nodes[0], w.nodes[0]=w.nodes[1]][0]
            w.azimut = w.azimut > 180 ? w.azimut-180 : w.azimut+180
        }
        next_node = junction.find(j => j.fid == w.nodes[1])        

        // Calcul de l'orientation relative du chemin
        difference = azimutDifference(azimut, w.azimut)
        azimut = w.azimut

        // Textualisation de l'orientation selon le cadrant horaire ou indication relative s'il y a lieu
        clock = Math.round(difference/30)%12
        var orientation;
        switch(clock) {
            case 0:
                orientation = "tout droit"
                break;
            case 3:
                orientation = "à droite"
                break
            case 6:
                orientation = "derrière"
                break
            case 9:
                orientation = "à gauche"
                break
            default:
                orientation = PP(P("à"), NP(NO(clock).dOpt({nat: true}),N("heure")))
        }

        /* Description */

        // Variation du début de phrase pour les arêtes suivantes
        start_p = "Marcher "
        if(!fl) {
            start_p = "Puis poursuivre "
        }

        // Indication de la position du prochain passage piéton
        if(w.type != "crossing_line" && next_node.type == "crossing_io") {

            content += "Le prochain passage piéton se trouve "+orientation+". "
        
        // Description du passage piéton
        } else if(w.type == "crossing_line") {
            switch(clock) {
                case 0:
                    orientation = "dans votre direction"
                    break;
                case 6:
                    orientation = "dans la direction opposée à la votre"
                    break;
                default:
                    orientation = PP(P("à"), NP(NO(clock).dOpt({nat: true}),N("heure")));
            }
            content += "Le passage piéton piéton est orienté "+orientation+". "

            /* Présence ou non d'un feu (sonore) */

            content += "Il" 
            content += w.traffic_light == "yes" ? " est protégé" : " n'est pas protégé"
            content += " par un feu"
            content += w.traffic_sound == "yes" ? " sonore" : ""

            content += " et"

            /* Présence ou non des bandes podotactiles aux extrémités du passage piéton et indication de leur qualité */

            if(current_node.tactile_paving == "yes" || next_node.tactile_paving == "yes") {
                if(current_node.tactile_paving == "yes" && next_node.tactile_paving == "no") {
                    content += " présente une bande podotactile"
                    content += current_node.tactile_paving_quality != "good" ? " dégradée" : ""
                    content += " au départ"
                } else if(current_node.tactile_paving == "no" && next_node.tactile_paving == "yes") {
                    content += " présente une bande podotactile"
                    content += current_node.tactile_paving_quality != "good" ? " dégradée" : ""
                    content += " à l'arrivée"
                } else {
                    content += " présente deux bandes podotactiles"
                    if(current_node.tactile_paving_quality != "good" && current_node.tactile_paving_quality != "good") {
                        content += " dégradées"
                    } else if(current_node.tactile_paving_quality == "good" && current_node.tactile_paving_quality == "good") {
                    } else {
                        if(current_node.tactile_paving_quality != "good") content += " dont la première"
                        if(next_node.tactile_paving_quality != "good") content += " dont la dernière"
                        content += " est dégradée"
                    }

                }

            } else {
                content += " ne présente pas de bande podotactile"
            }
            content += ". "

            /* Description des voies traversée */

            // Récupération des voies traversées
            crossed = []
            for(fid of Array.isArray(w.cross) ? w.cross : [w.cross]) {
                c = way.find(c => c.fid == fid)
                crossed.push(c)
            }

            // Comptage des voies traversées par type
            crossed_types = {}
            for(c of crossed) {
                if(crossed_types[c.type] == undefined) crossed_types[c.type] = 0
                crossed_types[c.type]+=1
            }

            // Récupération du type de la prochaine voie (trottoir, ilot)
            nw_type = way.find(nw => nw.fid != w.fid && (nw.nodes.includes(next_node.fid))).type

            content += "Il permet de traverser " +
            CP.apply(this,
                [C("et")].concat(
                    Array.from(
                        Object.entries(crossed_types), c => 
                        NP(
                            NO(c[1]).dOpt({nat: true}),
                            N("voie"),
                            PP(
                                P("de"),
                                NP(tr(c[0]))
                            ),
                        )
                    )
                )
            ) + " pour arriver sur " +
            NP(
                D("un"),
                N(tr(nw_type))
            ) + ". "
            
        }

        // Indication de la position du prochain poi
        else if(next_node.type == "poi") {

            content += start_p+orientation+" sur "+Math.floor(w.length)+" mètres jusqu'"+next_node.relative_position[0]+" "+PP(P("de"),NP(D("le"), N(next_node.relative_position[1]), Q(next_node.relative_position[2])))+". "
        
        } 

        else if(next_node.type == "start") {
            content += "Poursuivez alors votre route "+orientation+". "
        }

        // Indication de la position du prochain nœud non-référencé
        else {

            content += start_p+orientation+" sur "+Math.floor(w.length)+" mètres. "
        
        }

        fl = false;
        current_node = next_node
    }
    return([content, azimut])
}



