from yattag import Doc, indent

intersections  = QgsProject.instance().mapLayersByName("intersection")[0]
branches  = QgsProject.instance().mapLayersByName("branch")[0]
junctions = QgsProject.instance().mapLayersByName("junction")[0]
ways  = QgsProject.instance().mapLayersByName("way")[0]

doc, tag, text, line = Doc().ttl()
doc.asis("<?xml version=\"1.0\" encoding=\"utf-8\" ?>")

def layerToXml(layer):
    fields = layer.fields().names()
    for feature in layer.getFeatures():
        args = {"tag_name":layer.name()}
        for field in fields :
            if feature.attribute(field):
                args[field] = feature.attribute(field)
        doc.stag(**args)

with tag("intersection", type = intersections.getFeature(1).attribute("type")):
    layerToXml(branches)
    layerToXml(junctions)
    layerToXml(ways)
    
file = open("data.xml", "w")
file.write(indent(doc.getvalue()))
file.close()
