function XPathRequest(xml, req) {
    results = [];
    iterator = xml.evaluate(req, xml, null, XPathResult.ANY_TYPE, null);
    while(elt = iterator.iterateNext()) {
        results.push(elt);
    }
    return(results);
}

function unique(arr) {
    var uniques = [];
    var itemsFound = {};
    for(var i = 0, l = arr.length; i < l; i++) {
        var stringified = JSON.stringify(arr[i]);
        if(itemsFound[stringified]) { continue; }
        uniques.push(arr[i]);
        itemsFound[stringified] = true;
    }
    return uniques;
}

function azimutDifference(a, b) {
    if(a > b) {
        return(b+360-a)
    } else {
        return(b-a)
    }
}

function tr(word) {
    switch(word) {
        case "road":
            return "circulation"
            break;
        case "island":
            return "îlot"
            break;
        case "sidewalk":
            return "trottoir"
            break;
        case "cross":
            return "croix"
            break;
        case "star":
            return "étoile"
            break;
        default:
            return word
    }
}

function getNextNodes(node_id, map, nodes, types) {
    nextNodes = []
    visited = [node_id]
    toVisit = Object.entries(map[node_id]).map(m => Number(m[0]))
    for(i = 0; i < toVisit.length; i++) {
        id = toVisit[i]
        j = nodes.find(j => j.fid == id)
        visited.push(id)
        if(types.includes(j.type)) {
            nextNodes.push(j)
        } else {
            toVisit = toVisit.concat(Object.entries(map[id]).map(m => Number(m[0])).filter(n => visited.includes(n) == false && toVisit.includes(n) == false))
        }
    }
    return(nextNodes)
}

function getPathCost(map, path) {
    cost = 0
    for(i = 0; i < path.length-1; i++) {
        cost += map[path[i]][path[i+1]]
    }
    return(cost)
}

function pathAsWays(path, way) {
    path_as_ways = []
    for(i = 0; i < path.length-1; i++) {
        w = way.find(w => w.nodes.includes(parseInt(path[i])) && w.nodes.includes(parseInt(path[i+1])))
        path_as_ways.push(w)
    }
    return(path_as_ways)
}